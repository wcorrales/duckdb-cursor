0.1.0 2023-01-11
----------------

* Released initial version.

0.2.0 2024-06-18
----------------

* Prepared statements from tuple to list.
* Fix bug get as_dict description tuple to list

